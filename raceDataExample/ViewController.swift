//
//  ViewController.swift
//  raceDataExample
//
//  Created by Ilia Prokhorov on 20/05/2019.
//  Copyright © 2019 Demens Deum. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        test(crashing: false)
    }
    
    func test(crashing: Bool) {
        
        var veryLongString = ""
        
        for _ in 0...200 {
            veryLongString.append("A")
        }
        
        let slowDataWriter = SlowDataWriter()
        
        for _ in 0...1000000 {
            slowDataWriter.data = veryLongString.data(using: .utf8)
            
            if crashing == true {
                let printableDataWriter = slowDataWriter
                DispatchQueue.global(qos: .userInteractive).async {
                    print(printableDataWriter.data ?? "-")
                }
            }
            else {
                let printableDataWriter = slowDataWriter.asStruct()
                
                // Uncomment to check compile-time immutable state
                // printableDataWriter.data = "222".data(using: .utf8)
                
                DispatchQueue.global(qos: .userInteractive).async {
                    print(printableDataWriter.data ?? "-")
                }
            }
        }
    }
}

