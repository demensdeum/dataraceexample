//
//  SlowDataWriter.swift
//  raceDataExample
//
//  Created by Ilia Prokhorov on 20/05/2019.
//  Copyright © 2019 Demens Deum. All rights reserved.
//

import Foundation

class SlowDataWriter {
    
    var data: Data?
    
    func asStruct() -> SlowDataWriterStruct {
        let slowDataWriterStruct = SlowDataWriterStruct(data: data)
        return slowDataWriterStruct
    }
    
}
